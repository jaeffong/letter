class PostsController < ApplicationController
	before_action :authenticate_user!, only:[ :edit, :update, :destroy]
	before_action :set_post, only: [:show, :edit, :update, :destroy]

	def index
		@posts = Post.all
	end

	def 	eat
		@posts = Post.eat
	end

	def live
		@posts = Post.live
	end
	
	def culture
		@posts = Post.culture
	end
	
	def wear
		@posts = Post.wear
	end



	def show
		  respond_to do |format|
		    format.html # show.html.erb
		    format.xml  { render :xml => @post }
		  end
	end

	def new
		@post = Post.new
	end

	def create
		@post = Post.new(post_params)
		@post.user = current_user
		respond_to do |format|
		    if @post.save
		      format.html { redirect_to(@post,
		                    :notice => 'Post was successfully created.') }
		      format.xml  { render :xml => @post,
		                    :status => :created, :location => @post }
		    else
		      format.html { render :action => "new" }
		      format.xml  { render :xml => @post.errors,
		                    :status => :unprocessable_entity }
		    end
		  end
		end

	def edit
	end

	def update
		if @post.update(post_params)
				redirect_to @post
		else
			render 'edit'
		end
	end

	def destroy
		@post.destroy
		redirect_to posts_path
	end

	private

	def post_params
		params.require(:post).permit(:title, :content, :category, :image, :season)
	end

	def set_post
		@post = Post.find(params[:id])
	end
end
