Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  root 'login#welcome'
  
  #devise_for :users, controllers: {registrations: 'registrations'}
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", registrations: 'registrations'}


  resources :posts do
  	collection do
  		get :wear
  		get :eat
  		get :live
  		get :culture
  	end
  end

  post 'posts/wear'
  post 'posts/eat'
  post 'posts/live'
  post 'posts/culture'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
